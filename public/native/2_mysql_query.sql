SELECT mhs.mhs_nama FROM tb_mahasiswa mhs
JOIN tb_mahasiswa_nilai mhsn ON mhs.mhs_id=mhsn.mhs_id
JOIN tb_matakulia mk on mhsn.mk_id = mk.mk_id
AND mhsn.nilai = (SELECT MAX(nilai) FROM tb_mahasiswa_nilai 
WHERE mhs_id=mhs.mhs_id)
WHERE mk.mk_kode = 'MK303'