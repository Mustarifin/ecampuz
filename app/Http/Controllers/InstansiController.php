<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Instansi;
use App\Http\Requests\Instansi\StoreRequest;

class InstansiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $instansies = Instansi::when($request->has('keyword'), function($query) use ($request){
            $query->where('instansi', 'like', '%'.$request->keyword.'%');
        })
        ->paginate(10)->withQueryString();
        return view('pages.instansi.index', compact('instansies'));
    }

    public function create()
    {
        return view('pages.instansi.create');
    }

    public function store(StoreRequest $request)
    {
        Instansi::create($request->all());

        return redirect()->route('instansi')->with('message', 'Data berhasil disimpan');
    }

    public function edit(Instansi $instansi)
    {
        return view('pages.instansi.edit', compact('instansi'));
    }

    public function update(Instansi $instansi, StoreRequest $request)
    {
        $instansi->update($request->all());

        return redirect()->route('instansi')->with('message', 'Data berhasil diubah');
    }

    public function delete(Instansi $instansi)
    {
        $instansi->delete();

        return redirect()->route('instansi')->with('message', 'Data berhasil dihapus');
    }

    public function detail(Instansi $instansi)
    {
        return view('pages.instansi.detail', compact('instansi'));
    }
}
