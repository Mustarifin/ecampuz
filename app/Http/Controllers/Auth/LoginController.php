<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\LoginRequest;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('pages.auth.login');
    }

    public function login(LoginRequest $request)
    {
        $login = \Auth::attempt($request->getData());

        if (!$login) {
            return response()->json([
                'status' => false,
                'message' => "Password does't match"
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Login Successfully'
        ]);
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('login');
    }
}
