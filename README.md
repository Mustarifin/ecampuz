### Installing

A step by step series of examples that tell you how to get a development env running

1.  `$ git clone <your repo>`
2.  `$ composer install`
3. `$ preparing database on local`
4.  Create **.env** file as per **.env.example**. #REQUIRED line must be change
5.  `$ php artisan key:generate`
6.  `$ php artisan migrate --seed`
7.  `$ php artisan serve`
8.  Access from Browser `$ localhost:8000`
9. `$ login credential email : admin@ecampuz.co.id | password : admin`