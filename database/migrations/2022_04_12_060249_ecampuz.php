<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_mahasiswa');
        Schema::dropIfExists('tb_matakulia');
        Schema::dropIfExists('tb_mahasiswa_nilai');

        Schema::create('tb_mahasiswa', function (Blueprint $table) {
            $table->unsignedBigInteger('mhs_id')->nullable();
            $table->string('mhs_nim')->nullable();
            $table->string('mhs_nama')->nullable();
            $table->string('mhs_angkatan')->nullable();
        });

        Schema::create('tb_matakulia', function (Blueprint $table) {
            $table->unsignedBigInteger('mk_id')->nullable();
            $table->string('mk_kode')->nullable();
            $table->integer('mk_sks')->nullable();
            $table->string('mk_nama')->nullable();
        });

        Schema::create('tb_mahasiswa_nilai', function (Blueprint $table) {
            $table->unsignedBigInteger('mhs_nilai_id')->nullable();
            $table->unsignedBigInteger('mhs_id')->nullable();
            $table->unsignedBigInteger('mk_id')->nullable();
            $table->decimal('nilai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
