@if ($paginator->hasPages())
    <div>
        <ul class="pagination">
            @foreach($elements as $element)
                @if(is_array($element))
                    @foreach($element as $page => $url)
                    
                        @if ($page == $paginator->currentPage())
                        <li class="page-item active">
                            <a class="page-link" href="#">{{ $page }}<span class="sr-only">(current)</span></a>
                        </li>
                        @else
                        <li class="page-item"><a class="page-link" href="{{$url}}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </ul>
    </div>
@endif
