<x-app-layout>
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Instansi Tables</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Basic Tables</li>
                    </ul>
                    <a href="{{ route('instansi.create') }}" class="btn btn-success btn-sm">Create</a>
                </div>
            </div>
        </div>
        <!-- /Page Header -->
        @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 d-flex">
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h3 class="card-title mb-0">Instansi</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th>Instansi</th>
                                        <th>Deskripsi</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($instansies as $key => $instansi)
                                    <tr>
                                        <td>{{ $instansi->instansi }}</td>
                                        <td>{{ $instansi->deskripsi }}</td>
                                        <td class="d-flex">
                                            <a href="{{ route('instansi.edit', $instansi) }}" class="btn btn-warning btn-sm">Edit</a>
                                            <form action="{{ route('instansi.delete', $instansi) }}" method="post" onsubmit="return confirm('are you sure?')">
                                                @csrf
                                                @method('delete')
                                                <button class="btn btn-danger btn-sm" type="submit">delete</button>
                                            </form>
                                            <a href="{{ route('instansi.detail', $instansi) }}" class="btn btn-success btn-sm">view</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $instansies->links() }}
                    </div>
                </div>
            </div>
        </div>
    
    </div>
    <!-- /Page Content -->
    @section('style')
        <!-- Select2 CSS -->
        <link rel="stylesheet" href="{{ asset('assets/orange/css/select2.min.css') }}">
        
        <!-- Datetimepicker CSS -->
        <link rel="stylesheet" href="{{ asset('assets/orange/css/bootstrap-datetimepicker.min.css') }}">
    @endsection

    @section('scripts')
        <!-- Select2 JS -->
        <script src="{{ asset('assets/orange/js/select2.min.js') }}"></script>
        
        <!-- Datetimepicker JS -->
        <script src="{{ asset('assets/orange/js/moment.min.js') }}"></script>
        <script src="{{ asset('assets/orange/js/bootstrap-datetimepicker.min.js') }}"></script>
        
    @endsection
</x-app-layout>