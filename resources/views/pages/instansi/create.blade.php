<x-app-layout>
<div class="content container-fluid">
    <!-- Page Header -->
    <div class="page-header">
        <div class="row">
            <div class="col">
                <h3 class="page-title">Create Tables</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('instansi') }}">Instansi</a></li>
                    <li class="breadcrumb-item active">Create Tables</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /Page Header -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Create Inputs</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('instansi.store') }}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label class="col-form-label col-md-2">Instansi</label>
                            <div class="col-md-10">
                                <input type="text" name="instansi" class="form-control" value="{{ old('deskripsi') }}">
                                @if($errors->has('instansi'))
                                    <p class="text-danger">{{ $errors->first('instansi') }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-md-2">Deskripsi</label>
                            <div class="col-md-10">
                                <textarea rows="5" name="deskripsi" cols="5" class="form-control" placeholder="Enter text here">{{ old('deskripsi') }}</textarea>
                                @if($errors->has('deskripsi'))
                                    <p class="text-danger">{{ $errors->first('deskripsi') }}</p>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</x-app-layout>