<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InstansiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/instansi', [InstansiController::class, 'index'])->name('instansi');
Route::get('/instansi/create', [InstansiController::class, 'create'])->name('instansi.create');
Route::post('/instansi', [InstansiController::class, 'store'])->name('instansi.store');
Route::get('/instansi/{instansi}', [InstansiController::class, 'edit'])->name('instansi.edit');
Route::put('/instansi/{instansi}', [InstansiController::class, 'update'])->name('instansi.update');
Route::delete('/instansi/{instansi}', [InstansiController::class, 'delete'])->name('instansi.delete');
Route::get('/instansi/{instansi}/detail', [InstansiController::class, 'detail'])->name('instansi.detail');

Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login'])->name('login.submit');
Route::get('logout', [LoginController::class, 'logout'])->name('login.logout');